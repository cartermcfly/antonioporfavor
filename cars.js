var paco = {};

paco = (function() {

    var cars = [];

    function orderByTrademarkAsc(x, y) {
        return x.ageOfStarted - y.ageOfStarted;
    }

    function orderByTrademarkDesc(x, y) {
        return x.ageOfStarted + y.ageOfStarted;
    }

    function orderByNameAsc(x, y) {
        return ((x.trademark == y.trademark) ? 0 : ((x.trademark > y.trademark) ? 1 : -1));
    }

    function orderByNameDesc(x, y) {
        return ((x.trademark == y.trademark) ? 0 : ((x.trademark < y.trademark) ? 1 : -1));
    }

    function sortTrademark() {

        cars.sort(orderByTrademarkAsc);
        document.write("<h1 style=color:red;>Hola Jose Manué</h1><br><hr>");
        document.write("<h2>Order by ageOfStarted Asc</h2><br>");
        showCarsByTrademark();


        cars.sort(orderByTrademarkDesc);
        document.write("<h2>Order by ageOfStarted Desc</h2><br>");
        showCarsByTrademark();
    }

    function showCarsByTrademark() {

        cars.map((car) => {
            document.write('<table>' + '<tr>' + '<td>' + car.ageOfStarted + '</td>' + '<td>' + car.trademark + '</td>' + '</tr>' + '</table>' + '<hr>');
        });
    }

    function sortName() {

        cars.sort(orderByNameAsc);
        document.write("<h2>Order by Name Asc</h2><br>");
        showCarsByName();

        cars.sort(orderByNameDesc);
        document.write("<h2>Order by Name Desc</h2><br>");
        showCarsByName();

    }

    function showCarsByName() {

        for (var i = 0; i < cars.length; i++) {
            document.write('<table>' + '<tr>' + '<td>' + cars[i].trademark + '</td>' + '<td>' + cars[i].ageOfStarted + '</td>' + '</tr>' + '</table>' + '<hr>');
        }
    }

    function getCarsFromService() {
        service.getCars()
            .then(function(carsArray) {
                cars = carsArray;
                sortTrademark();
                sortName();
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    getCarsFromService();

})();
