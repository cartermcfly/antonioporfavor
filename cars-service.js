var service = {};

service = (function() {

    var getCars = function() {
        console.log('service -> getCars');
        var deferred = $.Deferred();

        $.get('http://www.mocky.io/v2/58ac8a6b100000e415514ba1', function(data) {
            if (data) {
                deferred.resolve(data.cars);
            } else {
                deferred.reject('No hay datos');
            }
        });

        return deferred.promise();
    }

    return {
        getCars: getCars
    };

})();
