  var json = [{
      'trademark': 'Ferrari',
      'ageOfstarted': '1980'
  }, {
      'trademark': 'Aston Martin',
      'ageOfstarted': '1920'
  }, {
      'trademark': 'Opel',
      'ageOfstarted': '1940'
  }, {
      'trademark': 'Renault',
      'ageOfstarted': '1960'
  }, {
      'trademark': 'BMW',
      'ageOfstarted': '1950'
  }, {
      'trademark': 'Audi',
      'ageOfstarted': '2000'
  }, {
      'trademark': 'Mercedes',
      'ageOfstarted': '1990'
  }, {
      'trademark': 'Citroën',
      'ageOfstarted': '1970'
  }];


  function OrderByTrademarkAsc(x, y) {
      return x.ageOfstarted - y.ageOfstarted;
  }

  function OrderByTrademarkDesc(x, y) {
      return x.ageOfstarted + y.ageOfstarted;
  }

  function OrderByNameAsc(x, y) {
      return ((x.trademark == y.trademark) ? 0 : ((x.trademark > y.trademark) ? 1 : -1));
  }

  function OrderByNameDesc(x, y) {
      return ((x.trademark == y.trademark) ? 0 : ((x.trademark < y.trademark) ? 1 : -1));
  }

  function sortTrademark() {

      json.sort(OrderByTrademarkAsc);
      console.log("_________________________");
      console.log("Order by ageOfstarted Asc");
      console.log("_________________________");
      document.write("<h1 style=color:red;>Hola Jose Manué</h1><br><hr>");
      document.write("<h2>Order by ageOfstarted Asc</h2><br>");
      showJSONTrademark();


      json.sort(OrderByTrademarkDesc);
      console.log("_________________________");
      console.log("Order by ageOfstarted Desc");
      console.log("_________________________");
      document.write("<h2>Order by ageOfstarted Desc</h2><br>");
      showJSONTrademark();

      function showJSONTrademark() {

          for (var i = 0; i < json.length; i++) {
              console.log(json[i].ageOfstarted + ' ' + json[i].trademark);
              document.write('<table>' + '<tr>' + '<td>' + json[i].ageOfstarted + '</td>' + '<td>' + json[i].trademark + '</td>' + '</tr>' + '</table>' + '<hr>');
          }
      }
  }

  function sortName() {

      json.sort(OrderByNameAsc);
      console.log("_________________________");
      console.log("Order by Name Asc");
      console.log("_________________________");
      document.write("<h2>Order by Name Asc</h2><br>");
      showJSONName();

      json.sort(OrderByNameDesc);
      console.log("_________________________");
      console.log("Order by Name Desc");
      console.log("_________________________");
      document.write("<h2>Order by Name Desc</h2><br>");
      showJSONName();

      function showJSONName() {

          for (var i = 0; i < json.length; i++) {
              console.log(json[i].trademark + ' ' + json[i].ageOfstarted);
              document.write('<table>' + '<tr>' + '<td>' + json[i].trademark + '</td>' + '<td>' + json[i].ageOfstarted + '</td>' + '</tr>' + '</table>' + '<hr>');
          }
      }
  }

  sortTrademark();
  sortName();
